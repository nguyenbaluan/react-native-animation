import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getJobsRequest: null,
  getJobsSuccess: ["data"],
  getJobsFailure: null,
});

export const JobTypes = Types;
export default Creators;
/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetchingJob: false,
  listJob: [],
});

/* ------------- Selectors ------------- */

/* ------------- Reducers ------------- */

export const getJobsRequest = state => state.merge({ fetchingJob: true });
export const getJobsSuccess = (state, { data }) =>
  state.merge({ fetchingJob: false, listJob: data });
export const getJobsFailure = state => state.merge({ fetchingJob: false });


export const reducer = createReducer(INITIAL_STATE, {

  [Types.GET_JOBS_REQUEST]: getJobsRequest,
  [Types.GET_JOBS_SUCCESS]: getJobsSuccess,
  [Types.GET_JOBS_FAILURE]: getJobsFailure,
});
