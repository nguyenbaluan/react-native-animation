import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";

import rootSaga from "../Sagas";
import { reducers } from "./";

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  // if (ReduxPersist.active) {
  //  const persistConfig = ReduxPersist.storeConfig
  //    finalReducers = persistReducer(persistConfig, reducers)
  //  }
  return {
    ...createStore(reducers, applyMiddleware(sagaMiddleware)),
    runSaga: sagaMiddleware.run(rootSaga)
  };
};

export default configureStore;
