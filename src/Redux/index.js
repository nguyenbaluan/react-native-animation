import { combineReducers } from "redux";

const reducers = combineReducers({
  lang: require('./LanguageRedux').reducer,
  job: require("./JobRedux").reducer
});

import rootSaga from "../Sagas";

export default () => {
  // If rehydration is on use persistReducer otherwise default combineReducers

  // if (ReduxPersist.active) {
  //   const persistConfig = ReduxPersist.storeConfig
  //   finalReducers = persistReducer(persistConfig, reducers)
  // }
  let { store } = configureStore(reducers, rootSaga);

  return store;
};
export { reducers };
