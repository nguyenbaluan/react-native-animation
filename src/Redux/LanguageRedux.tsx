import { createActions, createReducer } from "reduxsauce";
import Immutable from "seamless-immutable";
import I18n from "../I18n";
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setLanguage: ["lang"]
});

export const LangTypes = Types;

export default Creators;
/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  lang: I18n.currentLocale()
});

/* ------------- Selectors ------------- */

/* ------------- Reducers ------------- */

export const setLanguage = (state: any, action: any) => {
  const { lang } = action;

  return state.merge({ lang });
};

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_LANGUAGE]: setLanguage
});
