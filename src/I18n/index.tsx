import match from "match-values";
import I18n from "react-native-i18n";
import en from "./locales/en";
import vi from "./locales/vi";

I18n.fallbacks = true;
I18n.defaultLocale = "en";
I18n.locale = "en";
const localeKeys = Object.keys(en);
// choose a different default separator
// so it's allowed to use dots in i18n keys

I18n.defaultSeparator = "/";

I18n.translations = {
  en,
  vi
};
const t = (key?: string, params?: any): any => {
  // empty key
  if (!key) return "";

  if (/^.*\.$/g.test(key.trim())) {
    // tslint:disable-next-line
    console.info(`Ignore an incomplete key ${key}`);

    return "";
  }

  // not existing key
  if (!localeKeys.includes(key)) {
    if (process.env.NODE_ENV === "test") {
      return I18n.t(key, params);
    }
    // tslint:disable-next-line
    console.warn("Missing key", key);
  }

  return I18n.t(key, params);
};
const getOtherLanguage = (language: string) => {
  return match(language.toLowerCase(), {
    en: "vi",
    vi: "en"
  });
};
const setLocale = (locale: string) => {
  I18n.defaultLocale = locale;
  I18n.locale = locale;
};

export { t, getOtherLanguage, setLocale };
export default I18n;
