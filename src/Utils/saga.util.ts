const isSuccessWithMultipleRespone = (respones: any) => {
  return respones.reduce((acc: any, res: any) => {
    if (!(res.ok && res.status === 200)) return false;

    return acc;
  }, true);
};

const isSuccess = (res: any) => res.ok && res.status === 200;

export { isSuccessWithMultipleRespone, isSuccess };
