// a library to wrap and simplify api calls
import apisauce from "apisauce";

// our "constructor"
const create = (baseURL = "http://150.95.205.236:3000/api") => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      "Cache-Control": "no-cache"
    },
    // 2 second timeout...
    timeout: 2000
  });

  // jobs
  const getJobs = () => api.get("/jobs");

  return {
    // jobs
    getJobs
  };
};

// let's return back our create method as the default.
export default {
  create
};
