const Colors = {
  background: "#1F0808",
  clear: "rgba(0,0,0,0)",
  facebook: "#3b5998",
  transparent: "rgba(0,0,0,0)",
  silver: "#F7F7F7",
  steel: "#CCCCCC",
  error: "rgba(200, 0, 0, 0.8)",
  ricePaper: "rgba(255,255,255, 0.75)",
  frost: "#D8D8D8",
  cloud: "rgba(200,200,200, 0.35)",
  windowTint: "rgba(0, 0, 0, 0.4)",
  panther: "#161616",
  charcoal: "#595959",
  coal: "#2d2d2d",
  bloodOrange: "#fb5f26",
  snow: "white",
  ember: "rgba(164, 0, 48, 0.5)",
  fire: "#e73536",
  drawer: "rgba(30, 30, 29, 0.95)",
  eggplant: "#251a34",
  border: "#483F53",
  banner: "#5F3E63",
  text: "#E0D7E5",
  accent: "#ff2824",
  primary: "#f64e59",
  success: "#3bd555",
  disabled: "#cacaca",

  foreground: "#212121",
  alterForeground: "#707070",
  inverseForeground: "#ffffff",
  secondaryForeground: "#bcbcbc",
  hintForeground: "#969696",
  highlight: "#bcbcbc",

  alterBackground: "#f2f2f2",
  overlayBackground: "#00000057",
  neutralBackground: "#f2f2f2",
  fadedBackground: "#e5e5e5",

  twitter: "#41abe1",
  google: "#e94335",

  gradientBaseBegin: "#ff9147",
  gradientBaseEnd: "#ff524c",
  gradientVisaBegin: "#63e2ff",
  gradientVisaEnd: "#712ec3",
  gradientMasterBegin: "#febb5b",
  gradientMasterEnd: "#f24645",
  gradientAxpBegin: "#42e695",
  gradientAxpEnd: "#3bb2bb",

  faded: "#e5e5e5",
  icon: "#c2c2c2",
  neutral: "#f2f2f2",

  info: "#19bfe5",
  warning: "#feb401",
  danger: "#ed1c4d",

  starsStat: "#2ab5fa",
  tweetsStat: "#ffc61c",
  likesStat: "#5468ff",

  doughnutFirst: "#8a98ff",
  doughnutSecond: "#ffd146",
  doughnutThird: "#c2d521",
  doughnutFourth: "#ff6b5c",

  followersProgress: "#c2d521",

  followersFirst: "#b3e5fc",
  followersSecond: "#81d4fa",
  followersThird: "#4fc3f7",
  followersFourth: "#42a5f5",

  chartsAreaStroke: "#097fe5",
  chartsAreaFill: "#d6ecff"
};

export default Colors;
