import React from "react";
import { Provider } from "react-redux";
import configureStore from "./Redux/ConfigureStore";
import RootNavigation from "./RootNavigation";
// create our store

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <RootNavigation />
    </Provider>
  );
};

export default App;
