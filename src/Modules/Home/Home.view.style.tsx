import glamorous from "glamorous-native";
import { Colors } from "../../Themes";

const staticStyles: any = {
  flex: 1,
  justifyContent: "center",
  padding: 10,
  backgroundColor: Colors.silver
};
const dynamicStyles: any = (props: any) => ({
  alignItems: props.center ? "center" : "flex-end"
});
const ButtonGlamorous: any = glamorous.view(staticStyles, dynamicStyles);

const LogoReactNative: any = glamorous.image({
  height: 200,
  width: 200
});

const ChangeLanguage: any = glamorous.text({
  // padding: 15,
  textTransform: "uppercase",
  color: Colors.inverseForeground,
  fontWeight: "bold"
});

const WrapButton: any = glamorous.touchableOpacity({
  borderRadius: 5,
  padding: 8,
  marginTop: 10,
  backgroundColor: Colors.twitter
});

export { ButtonGlamorous, LogoReactNative, ChangeLanguage, WrapButton };
