// @ts-ignore
import withLifecycle from "@hocs/with-lifecycle";
import { connect } from "react-redux";
import { compose, setStatic, withHandlers } from "recompose";
import { t } from "../../I18n";
import ActionsJob from "../../Redux/JobRedux";
import ActionsLanguage from "../../Redux/LanguageRedux";
import { Colors, Fonts } from "../../Themes";
import handlers from "./Home.handler";
import HomeView from "./Home.view";

const mapStateToProps = (state: any) => ({
  listJob: state.job.listJob,
  language: state.lang.lang
});

const mapDispatchToProps = (dispatch: any) => ({
  getBlogs: () => {
    dispatch(ActionsJob.getJobsRequest());
  },
  changeLanguage: (lang: string) => {
    dispatch(ActionsLanguage.setLanguage(lang));
  }
});
const enhence = compose<any, any>(
  // withProps((owenerProps: any) => ({props: owenerProps})),
  setStatic("navigationOptions", {
    title: t("home.header.title"),
    headerStyle: {
      backgroundColor: Colors.bloodOrange
    },
    headerTintColor: Colors.inverseForeground,
    headerTitleStyle: {
      fontWeight: "bold",
      fontSize: Fonts.size.h5
    }
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withHandlers(handlers),
  withLifecycle({
    onDidMount: (props: any) => {
      props.getBlogs();
    }
  })
);

export default enhence(HomeView);
