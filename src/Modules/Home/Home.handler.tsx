import { getOtherLanguage, setLocale } from "../../I18n";
export default {
  CHANGE_LANGUAGE: (props: any) => async () => {
    const newLanguage = getOtherLanguage(props.language);

    await props.changeLanguage(newLanguage);
    await setLocale(newLanguage);
  }
};
