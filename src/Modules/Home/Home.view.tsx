import React, { SFC } from "react";
import { Text } from "react-native";
import { t } from "../../I18n";
import {
  ButtonGlamorous,
  ChangeLanguage,
  LogoReactNative,
  WrapButton
} from "./Home.view.style";
interface IProps {
  listJob: any;
  language: string;
  changeLanguage(param: string): void;
}

interface IState {
  innerState: any;
}

interface IHandler {
  ON_CLICK(): void;
  CHANGE_LANGUAGE(): void;
}

const HomeView: SFC<IProps & IState & IHandler> = props => {
  return (
    <ButtonGlamorous center={true} onPress={props.ON_CLICK}>
      {/* {props.listJob.map((job: any) => (
        <Text key={job.id}>{job.name}</Text>
      ))} */}
      <LogoReactNative source={require("../../assets/react-naitve-logo.png")} />
      <Text style={{ textAlign: "center" }}>{t("home.view.welcome")}</Text>
      <WrapButton onPress={props.CHANGE_LANGUAGE}>
        <ChangeLanguage>{props.language}</ChangeLanguage>
      </WrapButton>
    </ButtonGlamorous>
  );
};

export default HomeView;
