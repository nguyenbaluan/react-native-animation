import { put, call, all } from "redux-saga/effects";
import JobActions from "../Redux/JobRedux";
import {
  isSuccess
} from "../Utils";

export function* getJobList(api, actions) {
  try {
    const response = yield call(api.getJobs);
    if (isSuccess(response)) {
      yield put(JobActions.getJobsSuccess(response.data));
    } else {
      yield put(JobActions.getJobsFailure());
    }
  } catch (error) {
    yield put(JobActions.getJobsFailure());
  }
}
