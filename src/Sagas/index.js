import { all, takeLatest } from "redux-saga/effects";

import { JobTypes } from "../Redux/JobRedux";
import { getJobList } from "./JobSaga";

import Api from "../Services/Api";

const api = Api.create();

export default function* root() {
  yield all([takeLatest(JobTypes.GET_JOBS_REQUEST, getJobList, api)]);
}
