import { pathOr } from "ramda";
import React, { SFC } from "react";
import { connect } from "react-redux";
import { compose, lifecycle } from "recompose";
import I18n from "./I18n";
import AppNavigation from "./Navigation/AppNavigation";
import LanguageActions from "./Redux/LanguageRedux";
const RootNavigation: SFC<any> = props => {
  return <AppNavigation />;
};

const mapStateToProps = (state: any) => ({
  language: state.lang.lang
});
const mapDispatchToProps = {
  setLanguage: LanguageActions.setLanguage
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentWillMount() {
      const language = pathOr("en", ["props", "language"], this);
      // reset language state from storage

      I18n.defaultLocale = language;
      I18n.locale = language;
    }
  })
)(RootNavigation);
