import { createAppContainer, createStackNavigator } from "react-navigation";

import HomeView from "../Modules/Home";

const MyStack = createStackNavigator({
  Home: { screen: HomeView }
});

export default createAppContainer(MyStack);
